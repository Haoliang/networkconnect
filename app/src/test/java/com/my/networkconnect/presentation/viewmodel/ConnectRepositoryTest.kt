package com.my.networkconnect.presentation.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.my.networkconnect.domain.entities.Status
import com.my.networkconnect.domain.repositories.ConnectRepository
import com.my.networkconnect.presentation.state.RequestState
import com.my.networkconnect.presentation.state.ResponseStatus
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.reactivex.Scheduler
import io.reactivex.Single
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.plugins.RxJavaPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.mockito.Mock
import java.lang.Exception
import java.util.concurrent.Callable

class ConnectRepositoryTest {

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: ConnectViewModel

    @MockK(relaxed = true)
    private lateinit var repository: ConnectRepository

    private val successStatus = Status(status = 200, message = "")

    @Before
    fun setUp() {
        RxAndroidPlugins.setInitMainThreadSchedulerHandler { h: Callable<Scheduler?>? -> Schedulers.trampoline() }
        RxJavaPlugins.setIoSchedulerHandler { h: Scheduler? -> Schedulers.trampoline() }
        MockKAnnotations.init(this)

        viewModel = ConnectViewModel(repository)
    }

    @Test
    fun getPublicStatusSuccessTest() {

        every {
            repository.getPublicStatus()
        } returns Single.just(successStatus)

        val liveData = viewModel.getPublicStatusLiveData()
        viewModel.getPublicStatus()

        assertEquals(liveData.value, ResponseStatus(state = RequestState.Success, status = successStatus))
    }

    @Test
    fun getPublicStatusFailureTest() {

        every {
            repository.getPublicStatus()
        } returns Single.error(Exception())

        val liveData = viewModel.getPublicStatusLiveData()
        viewModel.getPublicStatus()

        assertEquals(liveData.value, ResponseStatus(state = RequestState.Failure,
            status = Status(status = 1000, message = Exception().toString())))
    }

    @Test
    fun getPrivateStatusSuccessTest() {

        every {
            repository.getPrivateStatus()
        } returns Single.just(successStatus)

        val liveData = viewModel.getPrivateStatusLiveData()
        viewModel.getPrivateStatus()

        assertEquals(liveData.value, ResponseStatus(state = RequestState.Success, status = successStatus))
    }

    @Test
    fun getPrivateStatusFailureTest() {

        every {
            repository.getPrivateStatus()
        } returns Single.error(Exception())

        val liveData = viewModel.getPrivateStatusLiveData()
        viewModel.getPrivateStatus()

        assertEquals(liveData.value, ResponseStatus(state = RequestState.Failure,
            status = Status(status = 1000, message = Exception().toString())))
    }
}