package com.my.networkconnect.data.repositories

import com.my.networkconnect.data.datasources.ConnectDataSource
import com.my.networkconnect.domain.entities.Status
import com.my.networkconnect.domain.repositories.ConnectRepository
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.reactivex.Single
import org.junit.Before
import org.junit.Test
import org.mockito.MockitoAnnotations

class ConnectRepositoryImplTest {

//    @get:Rule
//    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var repository : ConnectRepository

    @MockK(relaxed = true)
    private lateinit var publicDataSource: ConnectDataSource
    @MockK(relaxed = true)
    private lateinit var privateDataSource: ConnectDataSource

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
        repository = ConnectRepositoryImpl(publicDataSource, privateDataSource)
    }

    @Test
    fun getPublicConnectStateSuccess() {
        val data = Status(status = 200, message = "")

        every {
            publicDataSource.getStatus()
        } returns  Single.just(data)

        repository.getPublicStatus()!!.test().assertValue(data).dispose()
    }

    @Test
    fun getPublicConnectStateFailure() {
        val data = Status(status = 1000, message = Exception().toString())

        every {
            publicDataSource.getStatus()
        } throws Exception()

        repository.getPublicStatus()!!.test().assertValue(data).dispose()
    }

    @Test
    fun getPrivateConnectStateSuccess() {
        val data = Status(status = 200, message = "")

        every {
            privateDataSource.getStatus()
        } returns  Single.just(data)

        repository.getPrivateStatus()!!.test().assertValue(data).dispose()
    }

    @Test
    fun getPrivateConnectStateFailure() {
        val data = Status(status = 1000, message = Exception().toString())

        every {
            privateDataSource.getStatus()
        } throws Exception()

        repository.getPrivateStatus()!!.test().assertValue(data).dispose()
    }
}