package com.my.networkconnect.data.datasources

import com.my.networkconnect.domain.entities.Status
import io.reactivex.Single

interface ConnectDataSource {
    fun getStatus() : Single<Status>?
}