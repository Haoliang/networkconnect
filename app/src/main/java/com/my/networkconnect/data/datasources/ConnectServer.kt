package com.my.networkconnect.data.datasources

import com.my.networkconnect.domain.entities.Status
import io.reactivex.Single
import retrofit2.http.GET

interface ConnectServer {
    @GET("/status")
    fun getStatus(): Single<Status>?
}