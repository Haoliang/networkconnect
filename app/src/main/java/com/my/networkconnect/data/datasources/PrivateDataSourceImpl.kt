package com.my.networkconnect.data.datasources

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import com.my.networkconnect.domain.entities.Status
import io.reactivex.Single
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory


class PrivateDataSourceImpl :
    ConnectDataSource {
    private val retrofit = Retrofit.Builder()
        .baseUrl("http://192.168.2.2")
        .addConverterFactory(GsonConverterFactory.create())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .build()

    override fun getStatus() : Single<Status>? {
        val customServer = retrofit?.create(ConnectServer::class.java)
        return customServer!!.getStatus()
    }
}