package com.my.networkconnect.data.repositories

import com.my.networkconnect.data.datasources.ConnectDataSource
import com.my.networkconnect.domain.entities.Status
import com.my.networkconnect.domain.repositories.ConnectRepository
import io.reactivex.Single
import java.lang.Exception

class ConnectRepositoryImpl(
    private val publicDataSource: ConnectDataSource,
    private val privateDataSource: ConnectDataSource
) : ConnectRepository{
    override fun getPublicStatus(): Single<Status>? {
        return try{
            publicDataSource.getStatus()
        } catch (e:Exception) {
            Single.just(Status(status = 1000, message = e.toString()))
        }
    }

    override fun getPrivateStatus(): Single<Status>? {
        return try{
            privateDataSource.getStatus()
        } catch (e:Exception) {
            Single.just(Status(status = 1000, message = e.toString()))
        }
    }
}