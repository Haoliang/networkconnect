package com.my.networkconnect.presentation.state

import com.my.networkconnect.domain.entities.Status

enum class RequestState {
    Loading,
    Success,
    Failure,
}

data class ResponseStatus (
    val state : RequestState? = RequestState.Success,
    val status : Status? = null
)