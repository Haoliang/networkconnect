package com.my.networkconnect.presentation.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.my.networkconnect.domain.entities.Status
import com.my.networkconnect.domain.repositories.ConnectRepository
import com.my.networkconnect.presentation.state.RequestState
import com.my.networkconnect.presentation.state.ResponseStatus
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

class ConnectViewModel(private val connectRepository: ConnectRepository): ViewModel() {

    private val publicStatusLiveData : MutableLiveData<ResponseStatus> = MutableLiveData()
    private val privateStatusLiveData : MutableLiveData<ResponseStatus> = MutableLiveData()

    fun getPublicStatus() {
        publicStatusLiveData.value = ResponseStatus(state = RequestState.Loading)
        connectRepository.getPublicStatus()!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                { status ->
                    publicStatusLiveData.value = ResponseStatus(state = RequestState.Success, status = status)
                },
                { error ->
                    publicStatusLiveData.value = ResponseStatus(state = RequestState.Failure,
                        status = Status(status = 1000, message = error.toString()))
                }
            )
    }

    fun getPublicStatusLiveData(): LiveData<ResponseStatus> {
        return publicStatusLiveData
    }

    fun getPrivateStatus() {
        privateStatusLiveData.value = ResponseStatus(state = RequestState.Loading)
        connectRepository.getPrivateStatus()!!
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe (
                {status ->
                    privateStatusLiveData.value = ResponseStatus(state = RequestState.Success, status = status)},
                {error ->
                    privateStatusLiveData.value = ResponseStatus(state = RequestState.Failure,
                        status = Status(status = 1000, message = error.toString()))}
            )
    }

    fun getPrivateStatusLiveData(): LiveData<ResponseStatus> {
        return privateStatusLiveData
    }

}