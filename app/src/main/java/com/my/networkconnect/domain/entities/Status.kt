package com.my.networkconnect.domain.entities

data class Status (
    val status : Int,
    val message : String
)