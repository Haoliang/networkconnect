package com.my.networkconnect.domain.repositories

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.my.networkconnect.domain.entities.Status
import io.reactivex.Observable
import io.reactivex.Single


interface ConnectRepository {
    fun getPublicStatus(): Single<Status>?
    fun getPrivateStatus(): Single<Status>?
}