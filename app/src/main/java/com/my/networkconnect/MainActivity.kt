package com.my.networkconnect

import android.os.Bundle
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.my.networkconnect.data.datasources.PrivateDataSourceImpl
import com.my.networkconnect.data.datasources.PublicDataSourceImpl
import com.my.networkconnect.data.repositories.ConnectRepositoryImpl
import com.my.networkconnect.presentation.state.RequestState
import com.my.networkconnect.presentation.state.ResponseStatus
import com.my.networkconnect.presentation.viewmodel.ConnectViewModel
import kotlinx.android.synthetic.main.activity_main.*


class MainActivity : AppCompatActivity() {
    var connectViewModel: ConnectViewModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        init()
        findViewById<Button>(R.id.button_public).setOnClickListener {
            getPublicStatus()
        }
        findViewById<Button>(R.id.button_private).setOnClickListener {
            getPrivateStatus()
        }
    }

    private fun init() {
        val connectRepository = ConnectRepositoryImpl(PublicDataSourceImpl(), PrivateDataSourceImpl())
        connectViewModel = ConnectViewModel(connectRepository = connectRepository)

        connectViewModel?.getPublicStatusLiveData()?.observe(this, Observer {
            result -> updatePublicStatusLayout(result)
        })
        connectViewModel?.getPrivateStatusLiveData()?.observe(this, Observer {
                result -> updatePrivateStatusLayout(result)
        })
    }

    private fun updatePublicStatusLayout(result:ResponseStatus) {
        when(result.state) {
            RequestState.Success -> info_public.text = "Success, message = ${result.status!!.message}"
            RequestState.Loading -> info_public.text = "Loading"
            RequestState.Failure -> info_public.text = "Failure, message = ${result.status!!.message}"
        }
    }

    private fun updatePrivateStatusLayout(result:ResponseStatus) {
        when(result.state) {
            RequestState.Success -> info_private.text = "Success, message = ${result.status!!.message}"
            RequestState.Loading -> info_private.text = "Loading"
            RequestState.Failure -> info_private.text = "Failure, message = ${result.status!!.message}"
        }
    }

    private fun getPublicStatus() {
        connectViewModel?.getPublicStatus()
    }

    private fun getPrivateStatus() {
        connectViewModel?.getPrivateStatus()
    }
}